# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

#--------------------------------------------------#
#--------------------------------------------------#

*** API Reservation by Rogelio Carpio ***

ruby version: 2.5.8
rails version: 6.1.6

* git clone https://gitlab.com/rccarpio_0718/hometime.git

* cd hometime

* bundle install

* Setup database
    - host: <localhost>
    - username: <root>
    - password: <password>

* Rake
    - rake db:create db:migrate

* API endpoint
    - [POST] - /api/v1/reservations