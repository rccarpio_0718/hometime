class CreateReservations < ActiveRecord::Migration[6.1]
  def change
    create_table :reservations do |t|
      t.integer :guest_id
      t.string :code
      t.date :start_date
      t.date :end_date
      t.decimal :expected_payout_amount, :precision => 19, :scale => 4, :default => 0.0
      t.string :guest_email
      t.string :guest_first_name
      t.string :guest_last_name
      t.string :guest_phone_numbers
      t.decimal :listing_security_price_accurate, :precision => 19, :scale => 4, :default => 0.0
      t.string :host_currency
      t.integer :nights
      t.integer :number_of_guests
      t.string :status_type
      t.decimal :total_paid_amount_accurate, :precision => 19, :scale => 4, :default => 0.0
      t.string :created_by
      t.string :updated_by
      t.timestamps
    end
  end
end
