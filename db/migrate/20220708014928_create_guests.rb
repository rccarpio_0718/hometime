class CreateGuests < ActiveRecord::Migration[6.1]
  def change
    create_table :guests do |t|
      t.string :localized_description
      t.integer :number_of_adults
      t.integer :number_of_children
      t.integer :number_of_infants
      t.string :created_by
      t.string :updated_by
      t.timestamps
    end
  end
end
