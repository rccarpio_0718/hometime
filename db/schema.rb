# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_07_08_014928) do

  create_table "guests", charset: "utf8", force: :cascade do |t|
    t.string "localized_description"
    t.integer "number_of_adults"
    t.integer "number_of_children"
    t.integer "number_of_infants"
    t.string "created_by"
    t.string "updated_by"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "reservations", charset: "utf8", force: :cascade do |t|
    t.integer "guest_id"
    t.string "code"
    t.date "start_date"
    t.date "end_date"
    t.decimal "expected_payout_amount", precision: 19, scale: 4, default: "0.0"
    t.string "guest_email"
    t.string "guest_first_name"
    t.string "guest_last_name"
    t.string "guest_phone_numbers"
    t.decimal "listing_security_price_accurate", precision: 19, scale: 4, default: "0.0"
    t.string "host_currency"
    t.integer "nights"
    t.integer "number_of_guests"
    t.string "status_type"
    t.decimal "total_paid_amount_accurate", precision: 19, scale: 4, default: "0.0"
    t.string "created_by"
    t.string "updated_by"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

end
