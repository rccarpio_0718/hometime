class Reservation < ApplicationRecord
    belongs_to :guest

    # validates :guest_email, presence: true, uniqueness: { scope: :code }
    validates :guest_email, presence: true, uniqueness: true
    validates :code, presence: true, uniqueness: true

    validates :expected_payout_amount, presence: true
    validates :listing_security_price_accurate, presence: true
    validates :total_paid_amount_accurate, presence: true

    def create_record ( params )
        self.transaction do
            params_reservation = params.as_json(:except => [:guest_details])
            params_guest = params[:guest_details]
            guest = Guest.new
            guest.create_record(params_guest)  
            self.guest_id = guest.id
            self.update!(params_reservation)
        end
    end

    def update_record ( params )
        self.transaction do
            params_reservation = params.as_json(:except => [:guest_details, :code, :guest_email])
            params_guest = params[:guest_details]            
            if self.update!(params_reservation)
                self.guest.update!(params_guest)
            end
        end
    end
end