class ApplicationController < ActionController::API

    def param_default p1, p2
        p1 || p2 || nil
    end

    def no_of_days d1, d2
        (Date.parse(d2.to_s) - Date.parse(d1.to_s)).to_i
    end

end
