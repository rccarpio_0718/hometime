class Api::V1::ReservationsController < ApplicationController
    before_action :set_reservation, only: [:show, :update]

    def index
    end

    # POST /api/v1/reservations
    def create
        begin
            # Distinguish payloads
            payload1 = reservation_params
            payload2 = params

            # Parsing payloads
            phone_n = payload1[:guest_phone_numbers].kind_of?(Array) ? payload1[:guest_phone_numbers].join(',') : payload1[:guest_phone_numbers]
            rv_code = param_default(payload1[:code], payload2[:reservation_code])

            data = {
                code: rv_code,
                start_date: param_default(payload1[:start_date], payload2[:end_date]),
                end_date: param_default(payload1[:end_date], payload2[:end_date]),
                expected_payout_amount: param_default(payload1[:expected_payout_amount], payload2[:payout_price]),
                guest_email: param_default(payload1[:guest_email], payload2[:guest] ? payload2[:guest][:email] : ""),
                guest_first_name: param_default(payload1[:guest_first_name], payload2[:guest] ? payload2[:guest][:first_name] : ""),
                guest_last_name: param_default(payload1[:guest_last_name], payload2[:guest] ? payload2[:guest][:last_name] : ""),
                guest_phone_numbers: param_default(phone_n, payload2[:guest] ? payload2[:guest][:phone] : ""),
                listing_security_price_accurate: param_default(payload1[:listing_security_price_accurate], payload2[:security_price]),
                host_currency: param_default(payload1[:host_currency], payload2[:currency]),
                nights: param_default(payload1[:nights], payload2[:nights]),
                number_of_guests: param_default(payload1[:number_of_guests], payload2[:guests]),
                status_type: param_default(payload1[:status_type], payload2[:status]),
                total_paid_amount_accurate: param_default(payload1[:total_paid_amount_accurate], payload2[:total_price]),
                guest_details: {
                    localized_description: param_default( payload1[:guest_details] ? payload1[:guest_details][:localized_description] : "#{payload2[:guests]} guests", ""),
                    number_of_adults: param_default( payload1[:guest_details] ? payload1[:guest_details][:number_of_adults] : payload2[:adults], ""),
                    number_of_children: param_default( payload1[:guest_details] ? payload1[:guest_details][:number_of_children] : payload2[:children], ""),
                    number_of_infants: param_default( payload1[:guest_details] ? payload1[:guest_details][:number_of_infants] : payload2[:infants], "")
                }
            }

            # Check if the input dates is empty
            render :json => {error: "Invalid date input."} and return if data[:end_date] == "" || data[:start_date] == ""

            # Check if the date range is invalid
            render :json => {error: "Invalid date range, from (#{Date.parse(data[:start_date]).strftime('%B %d, %Y')}) and to (#{Date.parse(data[:end_date]).strftime('%B %d, %Y')})."} and return if Date.parse(data[:end_date]) < Date.parse(data[:start_date])

            # Check if input dates vs to number of nights 
            total_day_by_date = no_of_days(data[:start_date], data[:end_date])
            total_nights = data[:nights].to_i
            render :json => {error: "Number of nights (#{total_nights}) doesn't match to the input dates between #{Date.parse(data[:start_date]).strftime('%B %d, %Y')} to #{Date.parse(data[:end_date]).strftime('%B %d, %Y')}."} and return if total_day_by_date != total_nights

            # Guest count checking
            params_guest = data[:guest_details]
            total_guest = params_guest[:number_of_adults].to_i + params_guest[:number_of_children].to_i + params_guest[:number_of_infants].to_i
            number_of_guests = data[:number_of_guests].to_i
            render :json => {error: "Number of guest doesn't match to guest details, #{data[:number_of_guests].to_f} vs #{total_guest.to_f}."} and return if total_guest != number_of_guests

            # Price amount checking if tally
            header_price = data[:total_paid_amount_accurate].to_d
            detail_price = data[:expected_payout_amount].to_d + data[:listing_security_price_accurate].to_d
            render :json => {error: "Total paid amount doesn't match with the sum of accurate amount and payout amount, #{header_price.to_f} vs #{detail_price.to_f}."} and return if header_price != detail_price

            # Check if code and email is exist
            reservation = {}
            reservation = Reservation.find_by_code_and_guest_email(rv_code, data[:guest_email]) rescue nil
            response = status = {}

            if reservation.nil?
                # Create new record
                reservation = Reservation.new
                reservation.create_record(data)
            else
                # Updating existing record
                reservation.update_record(data)
            end

            # Check if having errors
            if reservation.errors.count == 0
                response = reservation
                status = :ok
            else
                response = reservation.errors
                status = :unprocessable_entity
            end
            render :json => response, :status => status
        rescue StandardError => e
            render :json => {error: $!}
        end
    end

    def show
        render :json => @reservation
    end

    private

        def set_reservation
            @reservation = Reservation.find(params[:id])
        end

        def reservation_params
            params.require(:reservation).permit(:code, 
                :start_date, 
                :end_date, 
                :expected_payout_amount, 
                :guest_email, 
                :guest_first_name, 
                :guest_last_name, 
                :listing_security_price_accurate, 
                :host_currency, 
                :nights, 
                :number_of_guests,
                :status_type, 
                :total_paid_amount_accurate,
                :guest_phone_numbers => [],
                :guest_details => [:localized_description, :number_of_adults, :number_of_children, :number_of_infants]            
            )
        end

end